#!/usr/bin/python

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import urllib.request
from urllib.request import urlopen

titulo = ""

def normalize_whitespace(text):
    "Remove redundant whitespace from a string"
    text = text.rstrip('\n')
    return text

class CounterHandler(ContentHandler):

    def __init__ (self):
        self.inContent = 0
        self.theContent = ""

    def startElement (self, name, attrs):
        if name == 'item':
            self.inContent = 1
        elif name == 'title':
            self.inContent = 1
        elif name == 'link':
            self.inContent = 1

    def endElement (self, name):
        value_link = ""
        global titulo
        if name == 'item':
            pass
        elif name == 'title':
            titulo = self.theContent
        elif name == 'link':
            value_link = self.theContent
            print("<h2><a href = " + value_link + ">"+titulo+"</a></h2>")
        if self.inContent:
            self.inContent = 0
            self.theContent = ""

    def characters (self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars

# --- Main prog
if len(sys.argv)<1:
    print("Usage: python practica.py")
    sys.exit(1)

# Load parser and driver
url = 'http://barrapunto.com/index.rss'
xmlFile= urllib.request.urlopen(url)

JokeParser = make_parser()
JokeHandler = CounterHandler()
JokeParser.setContentHandler(JokeHandler)

# Ready, set, go!
JokeParser.parse(xmlFile)
